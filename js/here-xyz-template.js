
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # \\
// # # # # # # # # # # # # # # # # # # # # # # # # #  Add your Access Token and Layers   # # # # # # # # # # # # # # # # # # # # # # # # # # # # # \\
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # \\

    //Token --> https://developer.here.com/documentation/studio/map_customization_suite_hlp/dev_guide/index.html
	function getnumber() {
		return "Your_XYZ_Token";
	}

	//Create layer --> https://xyz.api.here.com/maps/latest/documentation/here.xyz.html'
	//Data stored in XYZ Hub
	function createlayers() {
		//Layer 1
		displaylayer(getlayer("layer1", 3, 20, "spacecode", {zIndex: 0, type: "Polygon", strokeWidth:5, stroke: "orange", fill: "green", opacity: "0.7"}));
		//Layer 2
		displaylayer(getlayer("layer2", 3, 20, "spacecode", {zIndex: 0, type: "Polygon", strokeWidth:3, stroke: "blue", fill: "pink", opacity: "0.7"}));
		//More layers...
		//More about style --> https://xyz.api.here.com/maps/latest/documentation/here.xyz.maps.layers.TileLayer.Style.html
	}

// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # \\
// # # # # # # # # # # # # # # # # # # # # # # # # # # #  # Do not change this code  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # \\
// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # \\

	//Create display events
	function createdisplayevents() {
		//Generate base map
		window.display = new  here.xyz.maps.Map( document.getElementById("map"), {
			zoomLevel : startZoom,
			center: {
				longitude: longitude, latitude: latitude
			},
			layers: mylayers
		});

		//Event if the window is resized
		window.addEventListener('resize', function() {
			display.resize();
		});
	}

	//Add layers to the map
	function getlayer(layername, minzoom, maxzoom, spacetoken, layerstyle) {
		return new here.xyz.maps.layers.TileLayer({
			name: layername,
			min: minzoom,
			max: maxzoom,
			provider: new here.xyz.maps.providers.SpaceProvider({
				name: layername,
				level: minzoom,
				space: spacetoken,
				credentials: {
					access_token: YOUR_ACCESS_TOKEN
				}
			}),
			style: {
				styleGroups: {
					myStyle: [layerstyle,
					]},
				assign: function(feature){
					return "myStyle";
				}
			}
		})
	}
	
	//Display layer
	function displaylayer(layername) {
		display.addLayer(layername);
	}

	//Change base map
	function changebasemap() {
		var imageelement = document.getElementById("otherbasemap");
		var layertoadd = imageelement.title.toLowerCase();
		if (layertoadd == "satellite") {
			imageelement.title = "Dark mode";
			imageelement.src = "img/dark.png";
		} else if (layertoadd == "dark mode") {
			imageelement.title = "Satellite";
			imageelement.src = "img/sat.png";
		} else {
		//Nothing else
		}
		//Get actual position
		mycenter = display.getCenter();
		longitude = mycenter.longitude;
		latitude = mycenter.latitude;
		startZoom = display.getZoomlevel();
		
		//Remove previous base map
		display.destroy();
		//Get new base map
		getbasemap(layertoadd);
		createlayers();
	}

	//Base Map Display
	function getbasemap(maptype) {
		if (maptype == "dark mode") {
			//Dark layers to display from Open Street Map
			mylayers = [
				new here.xyz.maps.layers.MVTLayer({
					name   : 'dark mode',
					remote : {
						url : 'https://xyz.api.here.com/tiles/osmbase/256/all/{z}/{x}/{y}.mvt?access_token='+ YOUR_ACCESS_TOKEN
					},
					min : 1,
					max : 20,
					style : {
						backgroundColor: '#555555',
						strokeWidthZoomScale: function (level) {
							return level > startZoom ? 1 : level > 14 ? .5 : .25
						},
						styleGroups: {
							'earth'        : [{ zIndex: 1, type: 'Polygon', fill: '#555555' }, { zIndex: 5, type: "Text", stroke: "#000000", strokeWidth:4, fill:"#FFFFFF", textRef: "properties.name" }],
							'water'        : [{ zIndex: 2, type: 'Polygon', fill: '#353535' }],
							//'places'     : [{ zIndex: 5, type: "Text", stroke: "#000000", strokeWidth:3, fill:"#FFFFFF", textRef: "properties.name" }],
							'boundaries'   : [{ zIndex: 4, type: 'Line',  stroke: '#404040', strokeWidth: 2 }]
						},
						assign: function (feature, level)
						{
							var props = feature.properties;
							var kind  = props.kind;
							var layer = props.layer; // the data layer of the feature
							var geom  = feature.geometry.type;
							if (layer == 'water') {
								if (geom == 'LineString' || geom == 'MultiLineString') {
									return;
								}
							}                        
							return layer;
						}
					}
				})
			]			
		} else {
			//Image layer for displaying satellite images
			mylayers = [
            	new here.xyz.maps.layers.TileLayer({
        			name: 'satellite',
                    min: 1,
                    max: 20,
                    provider: new here.xyz.maps.providers.ImageProvider({
			        	name: 'satellite',
			            url : 'https://{SUBDOMAIN_INT_1_4}.mapcreator.tilehub.api.here.com/tilehub/wv_livemap_bc/png/sat/256/{QUADKEY}?access_token='+YOUR_ACCESS_TOKEN
			        })
				})
			]
		}
		//Display events
		createdisplayevents();
	}
